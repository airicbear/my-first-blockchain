import java.util.ArrayList;
import com.google.gson.GsonBuilder;
import blockchain.Block;

public class Main {
  
  public static ArrayList<Block> blockchain = new ArrayList<Block>();
  public static int difficulty = 5;
  
  public static void main(String[] args) {
    // Add our blocks to the blockchain ArrayList:
    blockchain.add(new Block("Hi, I'm the first block.", "0"));
    System.out.println("\nStarting to mine block 1...");
    blockchain.get(0).mineBlock(difficulty);

    blockchain.add(new Block("Yo, I'm the second block.", blockchain.get(blockchain.size() - 1).hash));
    System.out.println("\nStarting to mine block 2...");
    blockchain.get(1).mineBlock(difficulty);

    blockchain.add(new Block("Hey, I'm the third block.", blockchain.get(blockchain.size() - 1).hash));
    System.out.println("\nStarting to mine block 3...");
    blockchain.get(2).mineBlock(difficulty);

    System.out.println("\nValidating blockchain... ");
    System.out.println(isChainValid() ? "Blockchain is valid!" : "Invalid blockchain.");

    String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
    System.out.println("\nThe blockchain: ");
    System.out.println(blockchainJson);

    // Non-JSON output:
    // for (int i = 0; i < blockchain.size(); i++) {
    //   System.out.println(blockchain.get(i) + "\n");
    // }
  }

  /**
   * Validate each block in the chain by comparing current and previous hashes of each block
   */
  public static Boolean isChainValid() {
    Block currentBlock, previousBlock;

    // Loop through blockchain to check hash:
    for (int i = 1; i < blockchain.size(); i++) {
      currentBlock = blockchain.get(i);
      previousBlock = blockchain.get(i - 1);
      // Compare registered hash and calculated hash:
      if (!currentBlock.hash.equals(currentBlock.calculateHash())) {
        System.out.println("Current hash is not equal.");
        return false;
      }
      // Compare previous hash and registered previous hash
      if (!previousBlock.hash.equals(currentBlock.previousHash)) {
        System.out.println("Previous hash is not equal.");
        return false;
      }
    }
    return true;
  }

}